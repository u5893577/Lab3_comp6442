package pers.James.lab3;

public class Add extends Expression {
    Expression e1;
    Expression e2;

    public Add(Expression e1, Expression e2){
        this.e1 = e1;
        this.e2 = e2;
    }

    @Override
    public String show() {
        String a = "("+e1.show()+"+"+e2.show()+")";
        return a;
    }

    @Override
    public int evaluate() {
        int res = e1.evaluate()+e2.evaluate();
        return res;
    }
}
