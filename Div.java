package pers.James.lab3;

public class Div extends Expression {
    Expression e1;
    Expression e2;

    public Div(Expression e1, Expression e2){
        this.e1 = e1;
        this.e2 = e2;
    }
    @Override
    public String show() {
        return "("+e1.show()+"/"+e2.show()+")";
    }

    @Override
    public int evaluate() {
        return e1.evaluate()/e2.evaluate();
    }
}
