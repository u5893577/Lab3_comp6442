package pers.James.lab3;

public class Lit extends Expression {
    int num;

    public Lit(int num){
        this.num = num;
    }

    @Override
    public String show() {
        if (num < 0){
            return "("+String.valueOf(num)+")";
        }else {return String.valueOf(num);}

    }

    @Override
    public int evaluate() {
        return num;
    }
}
