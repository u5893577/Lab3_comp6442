package pers.James.lab3.lab3_q2;

import java.util.ArrayList;

public interface MyObserver {

    void update();
}
