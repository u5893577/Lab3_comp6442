package pers.James.lab3.lab3_q2;

import pers.James.lab3.lab3_q2.DialGUI;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import javax.swing.JComponent;

/*
 * DialR - a red nob for controlling a value that ranges between 0 and 1.
 * The dials value is changed as you drag the mouse horizontally.
 * Eric McCreath
 */

public class DialR extends Dial implements MouseMotionListener,
		MouseListener {
	ArrayList<MyObserver> observers;


	public DialR(DialGUI gui) {
		super(gui);
		observers = new ArrayList<MyObserver>();

	}

	@Override
	public void registerObserver(DialGUI gui) {
		observers.add(gui);
	}

	protected void paintComponent(Graphics gg) {
		super.paintComponent(gg, Color.red);
	}

}
