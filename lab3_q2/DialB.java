package pers.James.lab3.lab3_q2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import javax.swing.JComponent;

/*
 * DialB - a blue nob for controlling a value that ranges between 0 and 1.
 * The dials value is changed as you drag the mouse horizontally.
 * Eric McCreath
 */

public class DialB extends Dial implements MouseMotionListener,
		MouseListener {
	ArrayList<MyObserver> observers;
	public DialB(DialGUI gui) {
		super(gui);
		observers = new ArrayList<MyObserver>();
	}

	@Override
	public void registerObserver(DialGUI gui) {
		observers.add(gui);
	}


	@Override
	protected void paintComponent(Graphics gg) {
		super.paintComponent(gg, Color.blue);
	}

}
