package pers.James.lab3;


public class Demo {

    public static void main(String[] args) {
        Expression exp1 = new Lit(5);
        System.out.println(exp1.show() + " evaluates to " + exp1.evaluate());
        Expression exp2 = new Add(new Lit(1), new Lit(1));
        System.out.println(exp2.show() + " evaluates to " + exp2.evaluate());
        Expression exp3 = new Add(new Lit(3), new Add(new Lit(3), new Lit(-2)));
        System.out.println(exp3.show() + " evaluates to " + exp3.evaluate());
        Expression exp4 = new Sub(new Lit(3), new Sub(new Lit(3), new Lit(-2)));
        System.out.println(exp4.show() + " evaluates to " + exp4.evaluate());
        Expression exp5 = new Div(new Lit(3), new Sub(new Lit(3), new Lit(-2)));
        System.out.println(exp5.show() + " evaluates to " + exp5.evaluate());
    }
}
